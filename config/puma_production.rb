directory '/var/www/gt2/current'
rackup '/var/www/gt2/current/config.ru'
environment 'production'
daemonize false
pidfile '/var/www/gt2/shared/pids/puma.pid'
state_path '/var/www/gt2/shared/pids/puma.state'
stdout_redirect '/var/www/gt2/shared/log/puma-stdout.log', '/var/www/gt2/shared/log/puma-stderr.log'
threads 0, 32
workers 0
bind 'unix:///var/www/gt2/shared/sockets/puma.sock'
